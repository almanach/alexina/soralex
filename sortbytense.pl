#!/usr/bin/env perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

while (<>) {
  chomp($_);
  s/ / /g;
  @line = split(/\t/, $_);
  $line[2] =~ /^(?:([123])(sg|pl))?(.*)$/;
  $hash{$3}{$2}{$1}{$line[1]."\t".$line[2]} = 1;
}

for $tense (sort keys %hash) {
#  print "====== $tense ======\n";
  print "\n";
  for $number ("sg", "pl", "") {
    if (defined($hash{$tense}{$number})) {
      for $pers ("1", "2", "3", "") {
	if (defined($hash{$tense}{$number}{$pers})) {
	  for $output_line (sort keys %{$hash{$tense}{$number}{$pers}}) {
	    print $output_line."\n";
	  }
	}
      }
    }
  }
}
